﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonTest
{
    partial class Program
    {
        private static int[] toggle(int[] states)
        {
            int[] input = new int[states.Length + 2];
            Array.Copy(states, 0, input, 1, states.Length);
            int[] result = new int[states.Length];
            input[0] = 0;
            input[input.Length - 1] = 0;

            for (int i = 1; i < input.Length - 1; i++)
                result[i - 1] = input[i - 1] ^ input[i + 1];

            return result;

        }

        //METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
        public static int[] cellCompete(int[] states, int days)
        {
            var result = states;

            for (int i = 0; i < days; i++)
            {
                result = toggle(result);
            }

            return result;
        }


        public static int generalizedGCD(int num, int[] arr)
        {
            var min = arr.Min();
            for (int i = min; i > 0; i--)
            {
                if (arr.All(e => e % i == 0))
                {
                    return i;
                }
            }
            return 1;
        }
    }
}
