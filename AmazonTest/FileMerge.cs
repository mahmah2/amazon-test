﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonTest
{
    partial class Program
    {
        private static int minimumTimeCompute(int numOfSubFiles, int[] files, ref int sum)
        {
            if (files == null)
            {
                throw new ArgumentNullException("files array is null.");
            }

            if (numOfSubFiles < 2)
            {
                throw new ArgumentException("numOfSubFiles are too small.");
            }

            if (numOfSubFiles != files.Length)
            {
                throw new ArgumentException("numOfSubFiles doesn't match array length.");
            }

            if (files.Any(s => s <= 0))
            {
                throw new ArgumentException("Sizes should be positive integer.");
            }

            if (files.Length == 2)
            {
                sum += (files[0] + files[1]);

                return sum;
            }

            var ordered = files.OrderBy(s => s).ToArray();

            ordered[1] = ordered[0] + ordered[1];

            var result = new int[ordered.Length - 1];

            Array.Copy(ordered, 1, result, 0, result.Length);

            sum += result[0];

            return minimumTimeCompute(numOfSubFiles - 1, result, ref sum);
        }



        private static int minimumTime(int numOfSubFiles, int[] files)
        {
            var sum = 0;

            var ordered = minimumTimeCompute(numOfSubFiles, files, ref sum);

            return sum;
        }
    }
}
