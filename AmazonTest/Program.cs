﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AmazonTest
{
    partial class Program
    {
        private const int FLAG_CHECKED = 1;
        private const int FLAG_UNCHECKED = 0;
        private const int FLAG_NOROAD = 0;
        private const int FLAG_DESTINATION = 9;

        private static int findDestination(int[,] area, int[,] flags, int currentRow, int currentColumn)
        {
            if (currentRow < 0 || currentColumn < 0 ||   //too low index
                currentRow >= area.GetLength(0) || currentColumn >= area.GetLength(1) ||  //too high index
                area[currentRow, currentColumn] == FLAG_NOROAD ||  //no road to this cell
                flags[currentRow, currentColumn] == FLAG_CHECKED)   //already visited
            {
                return -1;
            }

            //printState(area, flags, currentRow, currentColumn, 0);

            if (area[currentRow, currentColumn] == FLAG_DESTINATION)
            {
                return 0; //we have arrived
            }

            flags[currentRow, currentColumn] = FLAG_CHECKED;
            var costs = new List<int>();
            var neighbors = new List<Tuple<int, int>>{
                new Tuple<int, int>( 0, 1 ),
                new Tuple<int, int>( 1, 0 ),
                new Tuple<int, int>( 0, -1 ),
                new Tuple<int, int>( -1, 0 ),
            };

            foreach (var neighbor in neighbors)
            {
                var thisCost = findDestination(area, flags, 
                    currentRow + neighbor.Item1, currentColumn + neighbor.Item2);

                if (thisCost >= 0)
                    costs.Add(thisCost);
            }

            flags[currentRow, currentColumn] = FLAG_UNCHECKED;

            if (costs.Count > 0)
            {
                return costs.Min() + 1;
            }

            return -1;
        }

        private static void printState(int[,] area, int[,] flags, int currentRow, int currentColumn, int cost)
        {
            for (int i = 0; i < flags.GetLength(0); i++)
            {
                for (int j = 0; j < flags.GetLength(1); j++)
                {
                    if (currentRow == i && currentColumn == j)
                        Console.Write("*,");
                    else
                        Console.Write(flags[i, j] + ",");
                }

                Console.Write("\t\t");

                for (int j = 0; j < flags.GetLength(1); j++)
                {
                    Console.Write(area[i, j] + ",");
                }
                Console.Write("\n");
            }

            Console.WriteLine($"current row:{currentRow} column:{currentColumn}");
            Console.WriteLine($"current cost:{cost}");
            Console.WriteLine("------------------------");
            Console.ReadLine();
        }

        public static int minimumDistance(int numRows, int numColumns, int[,] area)
        {
            var flags = new int[area.GetLength(0), area.GetLength(1)];

            return findDestination(area, flags, 0, 0);
        }


        static void Main(string[] args)
        {
            //cellCompete(new int[] {1,1,1,0,1,1,1,1}, 2);
            //Console.WriteLine(generalizedGCD(5, new int[] { 2,4,6,8,10 }));

            //Console.WriteLine(minimumTime(4, new int[] { 4,8,6,12 }));

            //var input = new int[3, 3]{
            //    { 1,0,0 },
            //    { 1,0,0 },
            //    { 1,9,1 },
            //};
            //Console.WriteLine(minimumDistance(3, 3, input));

            var input = new int[5, 5]{
                { 1,1,1,1,1 },
                { 1,0,0,0,1 },
                { 1,0,0,0,1 },
                { 1,0,0,0,1 },
                { 1,1,9,1,1 },
            };
            Console.WriteLine(minimumDistance(5, 5, input));

        }
    }
}
